/* -------------------------------------------------------------------- 	*/
/* Nom du fichier: dijkstra.hpp                                 			*/
/* Rôle: Fichier d'entête, Définition d'une classe qui gère le calcul       */
/*       du plus court chemin à l'aide de l'algorithme de Dijkstra   		*/
/* -------------------------------------------------------------------- 	*/

#include <string>
#include <climits>
#include <iostream>
#include <vector>
#include <algorithm>
#include <exception>

#define MAXIMUM_NODES 50
#define NUMBER_OF_SUCCESSORS 4

class DijkstraGraph {

	using DijkstraSolution_t = struct DijkstraSolution_t {        //  Structure représentant
																  // la solution du graphe
							   		int label[MAXIMUM_NODES];
							   		int father[MAXIMUM_NODES];
							   		int treated[MAXIMUM_NODES];
							   };

	private:
		int							number_of_nodes;
		int 						successors_number[MAXIMUM_NODES];  			
		int 						successors[MAXIMUM_NODES][NUMBER_OF_SUCCESSORS];
		int 						distance[MAXIMUM_NODES][NUMBER_OF_SUCCESSORS];
		DijkstraSolution_t			dijkstraSolution;

	public:

		/* -------------------------------------------------------------------- */
        /* read_graph_from_input méthode permettant de lire un fichier et       */
        /*                       d'extraire les différentes informations        */
		/*                       informations du graphe        					*/
        /* En entrée: fileName: le nom du fichier contenant les informatios du  */
        /*                      graphe                                          */
        /* -------------------------------------------------------------------- */
		void read_graph_from_input(std::string fileName);

		/* -------------------------------------------------------------------- */
        /* verify_source_dest_nodes méthode permettant de vérifier si les       */
        /*                       indices des noeds fournis sont valides et      */
		/*                       bien présent dans le graphe        			*/
        /* En entrée: from: premier indice de noeud à vérifier sa validité      */
        /*            to: deuxième indice de noeud à vérifier sa validité       */
        /* -------------------------------------------------------------------- */
		void verify_source_dest_nodes(int from, int to);

		/* -------------------------------------------------------------------- */
        /* get_node_with_minimum_label méthode pour récupérer le noeud avec le  */
        /*                       plus petit poids et qui n'a pas encore été     */
		/*                       traité        									*/
		/* En sortie: l'indice du noeud ayant le poids minimal 					*/
        /* -------------------------------------------------------------------- */
		int get_node_with_minimum_label();

		/* -------------------------------------------------------------------- */
        /* shortest_path_dijkstra méthode pour afficher le plus court chemin    */
        /*                       entre deux noeuds     							*/
		/*                       traité        									*/
		/* En entrée: from: indice du noeud de départ      						*/
        /*            to: indice du noeud d'arrivée      						*/
        /* -------------------------------------------------------------------- */
		void shortest_path_dijkstra(int from, int to);

		/* -------------------------------------------------------------------- */
        /* get_dijkstra_solution méthode qui retourne les indices des noeuds     */
        /*                       formant le plus court chemin entre les      	*/
		/*                       deux noeuds données en entrée, et qui dans le  */
		/*                       cas où il y a pas de solutions lance une   	*/
		/*                       exception										*/
		/* En entrée: from: indice du noeud de départ      						*/
        /*            to: indice du noeud d'arrivée      						*/
		/* En sortie: vecteur contenant les indices qui forment le plus cours  	*/
		/*			  chemin													*/
        /* -------------------------------------------------------------------- */
		std::vector<int> get_dijkstra_solution(int from, int to);

		/* -------------------------------------------------------------------- */
        /* show_dijkstra_solution méthode qui affiche le plus court chemin      */
        /*                       calculé ainsi que sa distance				    */
		/* En entrée: solution: vecteur contenant la liste des noeuds du plus   */
        /*            court chemin     											*/
        /* -------------------------------------------------------------------- */
		void show_dijkstra_solution(std::vector<int> solution);

		/* -------------------------------------------------------------------- */
        /* getSolution méthode qui retourne la solution calculée      			*/
		/* En sortie: retourne la solution calculée sous forme d'une structure  */
		/*			  DijkstraSolution_t										*/
        /* -------------------------------------------------------------------- */
		DijkstraSolution_t getSolution() const;
};





