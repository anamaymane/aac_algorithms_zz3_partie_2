/* --------------------------------------------------------------------  */
/* Nom du fichier: tests.cpp                                             */
/* Rôle: Tests unitaires pour tester les différentes fonctions utilisées */
/*       lors du calcul du plus court chemin avec l'algorithme de        */
/*       Dijkstra                                                        */
/* --------------------------------------------------------------------  */


#include "library/catch.hpp"
#include "dijkstra.hpp"

TEST_CASE("Test shortest path is valid") {

    DijkstraGraph           graph;
    std::string             filename = "test_ressources/input.txt";
    std::vector<int>        solution;
    std::vector<int>        predictedSolution;
    int                     from = 0;
    int                     to = 4;

    graph.read_graph_from_input(filename);
    graph.shortest_path_dijkstra(from, to);
    solution = graph.get_dijkstra_solution(from, to);

    predictedSolution = {0, 7, 6, 5, 4};
    REQUIRE( solution == predictedSolution);

    from = 6;
    to = 2;
    graph.shortest_path_dijkstra(from, to);
    solution = graph.get_dijkstra_solution(from, to);

    predictedSolution = {6, 5, 2};
    REQUIRE( solution == predictedSolution);

}

TEST_CASE("Test shortest distance is valid") {

    DijkstraGraph           graph;
    std::string             filename = "test_ressources/input.txt";
    std::vector<int>        solution;
    int                     predictedDistance;
    int                     distance;
    int                     from;
    int                     to;

    from = 0;
    to = 8;
    graph.shortest_path_dijkstra(from, to);
    solution = graph.get_dijkstra_solution(from, to);

    distance = graph.getSolution().label[solution[solution.size() - 1]];
    predictedDistance = 14;
    REQUIRE( distance == predictedDistance);

    from = 1;
    to = 8;
    graph.shortest_path_dijkstra(from, to);
    solution = graph.get_dijkstra_solution(from, to);

    distance = graph.getSolution().label[solution[solution.size() - 1]];
    predictedDistance = 10;
    REQUIRE( distance == predictedDistance);

}