/* -------------------------------------------------------------------- */
/* Nom du fichier: main.cpp                                             */
/* Rôle: Programme principale pour lire un graphe depuis un fichier     */
/*       donné en entrée et calculer le plus court chemin avec          */
/*       l'algorithme de Dijkstra                                       */
/* -------------------------------------------------------------------- */


#include "dijkstra.hpp"
#include <string>

void printNotice();

int main() {
    
    std::string             filename;
    int                     choosenOperation,
                            from,
                            to;
    DijkstraGraph           graph;
    std::vector<int>        solution;

    std::cout << "\n--------- Utilitaire Dijkstra ---------"<< std::endl;

    try{
        printNotice();
        std::cout << "\nVeillez saisir le chemin du fichier contenant les données du graphe:"<< std::endl;
        std::cin >> filename;
    
        do {
            
            graph.read_graph_from_input(filename);

            std::cout << "\n1) - Calculer le plus court chemin via dijkstra" << std::endl;
            std::cout << "2) - Quitter l'application" << std::endl;
            std::cin >> choosenOperation;
            
            switch(choosenOperation) {

                case 1: 
                        std::cout << "\nVeillez choisir un sommet de depart et un sommet d'arrivee" << std::endl;
                        std::cin >> from;
                        std::cin >> to;
                        graph.shortest_path_dijkstra(from, to);
                        solution = graph.get_dijkstra_solution(from, to);
                        graph.show_dijkstra_solution(solution);
                        break;

                case 2: 
                    std::cout << "\nFermeture de l'application..." << std::endl;
                    exit(0);
                    break;

                default:
                    std::cout << "\nVeuillez choisir une operation valide" << std::endl;
            }

        } while(choosenOperation != 2);
        
    }
    catch(std::string& message) {
        std::cout << message << std::endl;
    }
    
    return 0;
}

void printNotice() {
    std::cout << "\nNotice: le fichier des donnees du graphe doit avoir la forme suivante:" << std::endl;
    std::cout << "\n\tNombre de noeud\n\tnombre_de_successeur_du_noeud_0 successeur_noued_0 distance_successeur_noeud_0 ...."<< std::endl;
    std::cout << "\tnombre_de_successeur_du_noeud_1 successeur_noued_1 distance_successeur_noeud_1 ...."<< std::endl;
    std::cout << "\t ..."<< std::endl;
}