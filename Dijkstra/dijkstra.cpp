/* -------------------------------------------------------------------- */
/* Nom du fichier: dijkstra.cpp                                         */
/* Rôle: Fichier source, Implémentation des méthodes du fichier         */
/*       d'entête dijkstra.hpp                                          */
/* -------------------------------------------------------------------- */


#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "dijkstra.hpp"

void DijkstraGraph::read_graph_from_input(std::string fileName) {


    std::ifstream       fichier(fileName, std::ios_base::in);


    if(fichier.is_open() == false)
        throw std::string("\nErreur: Le fichier contenant les donnees du graphe est "
                          "invalide ou le chemin fourni n'est pas correcte\n");

    fichier >> this->number_of_nodes;

    for (int i = 0; i < this->number_of_nodes; i++) {

        fichier >> this->successors_number[i];

        for (int j = 0; j < this->successors_number[i]; j++) {
            fichier >> this->successors[i][j];
            fichier >> this->distance[i][j];
            if(this->distance[i][j] < 0)
                throw std::string("L'algorithme de Dijkstra ne peut etre applique"
                                   " que pour des graphes ayant que des arcs a longueur" 
                                   " positives");
        }
        
    }
 
    
}

void DijkstraGraph::verify_source_dest_nodes(int from, int to) {
    if(!(from >= 0 && from < this->number_of_nodes) ||
       !(to >= 0 && to < this->number_of_nodes))
            throw 
              std::string("La valeur du noeud source ou destination est invalide");
}

//Get node with minimum label and which hasn't been
//treated yet
int DijkstraGraph::get_node_with_minimum_label() {

    int         minIndex = -1,
                minValue = INT_MAX;

    for (int i = 0; i < this->number_of_nodes; i++) {
    
        if(dijkstraSolution.treated[i] == 0 && 
           dijkstraSolution.label[i] <= minValue) {
               minIndex = i;
               minValue = dijkstraSolution.label[i];
           }
    }

    return minIndex;
}


void DijkstraGraph::shortest_path_dijkstra(int from, int to) {

    verify_source_dest_nodes(from, to);

    int pivot = from;

    for (int i = 0; i < this->number_of_nodes; i++) {
        dijkstraSolution.label[i] = INT_MAX;
        dijkstraSolution.treated[i] = 0;
    }

    dijkstraSolution.label[pivot] = 0;
    dijkstraSolution.father[pivot] = -1;

    

    //Verify if we have nodes that haven't been treated
    while (pivot != -1) {
        
        dijkstraSolution.treated[pivot] = 1;

        //Treat all pivot succesors
        for(int j = 0; j < this->successors_number[pivot]; j++) {

            if(dijkstraSolution.treated[this->successors[pivot][j]] == 0) {

                int x = this->successors[pivot][j];

                if(dijkstraSolution.label[x] > dijkstraSolution.label[pivot] + this->distance[pivot][j]) {
                    dijkstraSolution.label[x] = dijkstraSolution.label[pivot] + this->distance[pivot][j];
                    dijkstraSolution.father[x] = pivot;
                }
            }
        }

        pivot = get_node_with_minimum_label();
    }
}


std::vector<int> DijkstraGraph::get_dijkstra_solution(int from, int to) {

    int                  father = dijkstraSolution.father[to];
    std::vector<int>     solution;

    if(dijkstraSolution.label[to] == INT_MAX) {
        std::stringstream message;
        message << "There is no way to go from "  << from << " to " << to;
        throw message.str();
    }
    solution.push_back(to);

    while(father != -1) {
        solution.push_back(father);
        father = dijkstraSolution.father[father];
    }

    std::reverse(solution.begin(), solution.end());

    return solution;

}

void DijkstraGraph::show_dijkstra_solution(std::vector<int> solution) {

    std::cout << "\n---------\n\nChemin: ";

    for(int i = 0; i < solution.size(); i++) {
        if(i != solution.size() - 1)
            std::cout << solution[i] << " -> ";
        else
            std::cout << solution[i] << "\n\ndistance: " << dijkstraSolution.label[solution[i]] << "\n\n---------\n"<<std::endl;
    }
}

DijkstraGraph::DijkstraSolution_t DijkstraGraph::getSolution() const {
    return this->dijkstraSolution;
}