### Dijkstra

1. **$_> cd Dijkstra**
2. **$_> make test**
3. **$_> ./dijkstra_test** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//Pour lancer les tests
4. **$_> make**
5. **$_> ./dijkstra** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//Pour lancer le programme principale du calcul du plus court chemin

### Eulerien

1. **$_> cd Eulerien/build**
2. **$_> cmake ../CMakeLists.txt**
3. **$_> cd ../**
4. **$_> make**
5. **$_> ./test**
