#ifndef __EULERIEN__HPP
#define __EULERIEN__HPP

#include <iostream>
#include <list>
#include <cassert>

using namespace std;


/*******************************************************************************************
* @brief Graph_t : this class represente the structure of a graph
* we found number of vertex and the adjacents of each vertex
*******************************************************************************************/
class Graph
{
    private:
        list<int> * vertices; // An array of adjacency lists  
        int         nbVertex; // Number of vertex in a graph    

    public:

        /******************************************************************************************
        * @brief Graph_t : the constructor
        * 
        * input    : number of vertex of a graph
        * output   : a Graph object
        ******************************************************************************************/
        Graph(int nbVertex);

        /******************************************************************************************
        * @brief getVertices : return the list of adjacent
        * 
        * input : void
        * output: array of lists of adajacent
        *
        ******************************************************************************************/
        list<int> * getVertices();

        /******************************************************************************************
        * @brief addEdge : added an edge to the matrix of adjacents for each vertex
        * 
        * input : 
        *      vertex   : a vertex number, 
        *      edge     : a edge number.
        *
        * output: void
        *
        ******************************************************************************************/
        void addEdge(int vertex, int edge);


        /******************************************************************************************
        * @brief DFS : epth-First-Search, * it'a a recusrsive 
        * function
        * 
        * input : 
        *      currentVertix    : a vertex, 
        *      visited_vertex   : an array to check if a element
        *                        is visited or not.
        *
        * output: void
        *
        ******************************************************************************************/
        void dfs(int currentVertix, bool isVertixVisited[]);


        /******************************************************************************************
        * @brief isConnected: verifiy if the graph is connected
        * 
        * input : void
        * output: boolean whether or not the graph is connected
        *
        ******************************************************************************************/
        bool isConnected();

        /******************************************************************************************
        * @brief isVerticesDegreesAreEven: verifiy If all the vertices have even degree
        * 
        * input : void
        * output: boolean whether or not the vertices have even degree
        *
        ******************************************************************************************/
        bool isVerticesDegreesAreEven();

        /******************************************************************************************
        * @brief isGraphEulerian: verifiy If the graph has an Euler circuit
        * 
        * input : void
        * output: boolean whether or not he graph has an Euler circuit
        *
        ******************************************************************************************/
        bool isGraphEulerian();

        /******************************************************************************************
        * @brief printGraph: print each vertix and his adjacent
        * 
        * input : void
        * output: print each vertix and his adjacent
        *
        ******************************************************************************************/
        void printGraph();

};

#endif
