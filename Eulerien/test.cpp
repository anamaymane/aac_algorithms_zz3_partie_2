
#include "eulerien.hpp"
#include "catch.hpp"
#include <iterator>


TEST_CASE("Test if the graph values are correct")
{
    Graph graph(2);

    list<int> * vertices = new list<int>[2];
    for(int i=0; i < 2; i++)
        for(int j = 0;  j < 3; j++)
            vertices[i].push_back(j);
    
    graph.addEdge(0, 0);
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 1);
    graph.addEdge(1, 2);


    list<int> * graphVertices = graph.getVertices();
    for(int i=0; i < 2; i++)
    {
        list<int>::iterator ItGraphVertices = graphVertices[i].begin();
        list<int>::iterator ItVertices = vertices[i].begin();
        for(int j = 0;  j < 3; j++)
        {
            advance(ItGraphVertices, j);
            advance(ItVertices, j);
            int graphAdjValue = *ItGraphVertices;
            int adjValue = *ItVertices;

            REQUIRE( graphAdjValue == adjValue);
        }       
    }
}

TEST_CASE("Test if the graph is not connected")
{
    Graph graph(4);
    
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 2);

    graph.addEdge(2, 1);
    graph.addEdge(2, 0);
    
    REQUIRE( graph.isConnected() == false);
}

TEST_CASE("Test if the graph is connected")
{
    Graph graph(4);
    
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 2);
    graph.addEdge(1, 3);

    graph.addEdge(2, 1);
    graph.addEdge(2, 0);
    
    REQUIRE( graph.isConnected() == true);
}


TEST_CASE("Test if all vertices don't have an even degrees")
{
    Graph graph(4);
    
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 2);
    graph.addEdge(1, 3);

    graph.addEdge(2, 1);
    graph.addEdge(2, 0);
    
    REQUIRE( graph.isVerticesDegreesAreEven() == false);
}

TEST_CASE("Test if all the vertices have an even degrees")
{
    Graph graph(4);
    
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 3);

    graph.addEdge(2, 0);
    graph.addEdge(2, 3);

    graph.addEdge(3, 1);
    graph.addEdge(3, 2);
    
    REQUIRE( graph.isConnected() == true);
}

TEST_CASE("Test if the graph is Eulerian")
{
    Graph graph(4);
    
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 3);

    graph.addEdge(2, 0);
    graph.addEdge(2, 3);

    graph.addEdge(3, 1);
    graph.addEdge(3, 2);
    
    REQUIRE( graph.isGraphEulerian() == true);
}

TEST_CASE("Test if the graph not Eulerian")
{
    Graph graph(4);
    
    graph.addEdge(0, 1);
    graph.addEdge(0, 2);

    graph.addEdge(1, 0);
    graph.addEdge(1, 2);
    graph.addEdge(1, 3);

    graph.addEdge(2, 1);
    graph.addEdge(2, 0);
    
    REQUIRE(graph.isGraphEulerian() == false);
}


TEST_CASE("Final test")
{
    Graph   graph_1(6),
            graph_2(6),
            graph_3(1);

    graph_1.addEdge(0, 1);
    graph_1.addEdge(0, 2);
    graph_1.addEdge(1, 3);
    graph_1.addEdge(2, 3);
    graph_1.addEdge(2, 4);
    graph_1.addEdge(3, 4);
    graph_1.addEdge(3, 5);
    graph_1.addEdge(4, 5);
    

    graph_2.addEdge(0, 1);
    graph_2.addEdge(0, 2);
    graph_2.addEdge(1, 3);
    graph_2.addEdge(2, 3);
    graph_2.addEdge(2, 4);
    graph_2.addEdge(3, 5);
    graph_2.addEdge(4, 5);

    REQUIRE(graph_1.isGraphEulerian() == false);
    REQUIRE(graph_2.isGraphEulerian() == false);
    REQUIRE(graph_3.isGraphEulerian() == true);
}