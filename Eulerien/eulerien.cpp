#include "eulerien.hpp"


Graph::Graph(int nbVertex)
{
    this->nbVertex  = nbVertex;
    vertices        = new list<int>[this->nbVertex];
}

list<int> * Graph::getVertices()
{
    return this->vertices;
}

void Graph::addEdge(int vertex, int edge)
{
    this->vertices[vertex].push_back(edge);
}

void Graph::printGraph()
{

    for(int i = 0; i < this->nbVertex; i++)
    {
        list<int>::iterator itEdges = vertices[i].begin();
        list<int>::iterator itEdgesEnd = vertices[i].end();

        cout << "Vertix("<< i << ") : ";
        for(; itEdges != itEdgesEnd;  itEdges++)
        {
            cout << *itEdges << ", ";
        }
        cout << endl;
    }
}

void Graph::dfs(int currentVertix, bool isVertixVisited[])
{
    isVertixVisited[currentVertix] = true;

    // we check if every vertix adjacent is visited or not. using a recursive function
    for(auto adj : this->vertices[currentVertix])
        if(!isVertixVisited[adj])
            dfs(adj, isVertixVisited);
}


bool Graph::isConnected()
{
    bool * isVertixVisited  = new bool[this->nbVertex];
    int initialVertex       = 0;

    fill(isVertixVisited, isVertixVisited + this->nbVertex, false);

    dfs(initialVertex, isVertixVisited);

    int i = 0;
    while(i < this->nbVertex && isVertixVisited[i] == true) i++;

    // if all the vertices are visited, that's mean the graph is connected
    return i == this->nbVertex;
}


bool Graph::isVerticesDegreesAreEven()
{
    int i = 0;
    while( i < this->nbVertex && this->vertices[i].size()%2 == 0 ) i++;

    return i == this->nbVertex;
}

bool Graph::isGraphEulerian()
{
    return (this->isConnected() == true && this->isVerticesDegreesAreEven() == true);
}